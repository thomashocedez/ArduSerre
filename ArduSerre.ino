
#include "Adafruit_WS2801.h"
#include <SPI.h>
#include <Adafruit_GFX.h>
#include <Adafruit_PCD8544.h>
// Hardware SPI (faster, but must use certain hardware pins):
// SCK is LCD serial clock (SCLK) - this is pin 13 on Arduino Uno
// MOSI is LCD DIN - this is pin 11 on an Arduino Uno
// pin 5 - Data/Command select (D/C)
// pin 4 - LCD chip select (CS)
// pin 3 - LCD reset (RST)
Adafruit_PCD8544 display = Adafruit_PCD8544(5, 4, 3);
#include <DS1302.h>
#include <Wire.h>            // linked to RTC
DS1302 rtc(10, 7, 6);
// DS1302:  CE pin    -> Arduino Digital 2
//          I/O pin   -> Arduino Digital 3
//          SCLK pin  -> Arduino Digital 4

#define PIN_M 0 // Moisture Sensor
#define PIN_L 1 // LDR = light sensor

#include <SimpleDHT.h>
int pinDHT11 = 8;

SimpleDHT11 dht11;

uint8_t dataPin  = 12;    // Yellow wire on Adafruit Pixels
uint8_t clockPin = 2;    // Green wire on Adafruit Pixels

Adafruit_WS2801 strip = Adafruit_WS2801(4, dataPin, clockPin);

byte sen_h = 0; //Relative humidity
byte sen_t = 0; // Temperature
int sen_m = 0; // Moisture
int sen_l = 0; // Light level

bool thirst = false;
bool night = false;
bool cold = false;

long time;
String myDate;
String myTime;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  strip.begin();

  // Update LED contents, to start they are all 'off'
  strip.show();
  pinMode(6, OUTPUT);
  pinMode(9, OUTPUT);
  pinMode(pinDHT11, INPUT);
  pinMode(2, OUTPUT);
  display.begin();
  display.setContrast(50);
  display.display();
}

void get_sensors() {
  int err = SimpleDHTErrSuccess;
  sen_m = map(analogRead(PIN_M), 0, 1023, 100, 0);
  sen_l = map(analogRead(PIN_L), 0, 1023, 100, 0);
  if ((err = dht11.read(pinDHT11, &sen_t, &sen_h, NULL)) != SimpleDHTErrSuccess) {
    return;
  }

}

void update_status() {

  if (sen_m <= 20) {
    thirst = true;
  } else {
    thirst = false;
  }

  if (sen_l <= 15) {
    night = true;
  } else {
    night = false;
  }

  if (sen_t >= 18) {
    cold = false;
  } else {
    cold = true;
  }

  if (thirst) {
    digitalWrite(9, LOW);
  } else {
    digitalWrite(9, HIGH);
  }

  if (night) {
    for (int i = 0; i < strip.numPixels(); i++) {
      strip.setPixelColor(i, Color(200, 250, 100));
    }
  } else {
    for (int i = 0; i < strip.numPixels(); i++) {
      strip.setPixelColor(i, Color(0, 0, 0));
    }

  }    strip.show();

}

void display_status() {
  display.clearDisplay();

  display.setCursor(0, 0);
  display.print("Temp: ");
  display.print(sen_t, 1);

  display.setCursor(0, 8);
  display.print("Humi: ");
  display.print(sen_h, 1);

  display.setCursor(0, 16);
  display.print("Mois: ");
  display.print(sen_m, 1);

  display.setCursor(0, 24);
  display.print("Lumi: ");
  display.print(sen_l, 1);

  display.setCursor(0, 32);
  String rtcs = rtc.getTimeStr();
  display.print(rtcs);

  display.display();


}
void loop() {

  get_sensors();

  update_status();

  display_status();

  delay(5000);

}

uint32_t Color(byte r, byte g, byte b)
{
  uint32_t c;
  c = r;
  c <<= 8;
  c |= g;
  c <<= 8;
  c |= b;
  return c;
}